import os
import sys
import shutil
import subprocess

def runCmd(arg):
    print (arg)
    cmd = subprocess.Popen(arg)
    runcode = cmd.wait()  

def GetOfCmd(arg):
    print (arg)
    cmd = subprocess.Popen(arg, stdout=subprocess.PIPE)
    cmd.wait() 
    return cmd.stdout.read()
    
def SetInCmd(arg, str_key):
    print (arg)
    cmd = subprocess.Popen(arg, stdin=subprocess.PIPE, shell=True)
    cmd.communicate(input=str_key.encode())
    cmd.wait()     
   
def main():
    path_str = None
    package_str = None    
    apk_name = "testApp.apk"
    path_to_unpack_apk = "./unpacked/dist/" + apk_name
    path_to_apk = "./testApp/"
    if not os.path.exists(path_to_apk):
        os.makedirs(path_to_apk) 
    path_to_apk += apk_name
    
    for i in range(1, len(sys.argv)):
        if "--path" in sys.argv[i]:
            path_str = sys.argv[i+1]
        elif "--package" in sys.argv[i]:
            package_str = sys.argv[i+1]
    if package_str:    
        apk_path = GetOfCmd(["adb", "shell", "pm", "path", package_str]).decode('ascii').splitlines()[0].split(":")[1]
        runCmd(["adb", "pull", apk_path, path_to_apk])
    elif path_str:
        shutil.copyfile(path_str, path_to_apk)
    else:
        sys.exit(-1)
    
    runCmd(["apktool.bat", "d", "-d", "-f", "-o", "unpacked", path_to_apk])
    runCmd(["apktool.bat", "b", "-d", "unpacked"])  
    SetInCmd(["jarsigner", "-verbose", "-sigalg", "SHA1withRSA", "-digestalg", "SHA1", "-keystore", "./certificate/my-debug-key.keystore", path_to_unpack_apk, "alias_name"], "testApp")
    path_to_res = "./result/"
    if not os.path.exists(path_to_res):
       os.makedirs(path_to_res)
    path_to_res += "out.apk"
    shutil.copyfile(path_to_unpack_apk, path_to_res)
    print ("ready!")
    print ("see: " + path_to_res)
    
if __name__ == "__main__":
    main()